

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class stringCalculatorTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class stringCalculatorTest
{
    /**
     * Default constructor for test class stringCalculatorTest
     */
    public stringCalculatorTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    /**
     * String vacio retorna 0
     */
    @Test
    public void stringVacioRetornaCero(){
        String lista = "";
        int resultadoEsperado = 0;
        stringCalculator calculadora = new stringCalculator();
        int resultadoObtenido = calculadora.sumar(lista);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    /**
     * un numero retorna el numero
     */
    @Test
    public void unNumeroRetornaElNumero(){
        String lista = "66";
        int resultadoEsperado = 66;
        stringCalculator calculadora = new stringCalculator();
        int resultadoObtenido = calculadora.sumar(lista);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    /**
     * retorna la suma de 2 numeros separados por coma
     */
    @Test
    public void sumaNumeroEnComa(){
        String lista = "100,-200";
        int resultadoEsperado = -100;
        stringCalculator calculadora = new stringCalculator();
        int resultadoObtenido = calculadora.sumar(lista);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
    /**
     * retorna la suma de 3 numeros
     */
    @Test
    public void sumaNumeroEnComa2(){
        String lista = "100,200,-100";
        int resultadoEsperado = 200;
        stringCalculator calculadora = new stringCalculator();
        int resultadoObtenido = calculadora.sumar(lista);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
     /**
     * retorna la suma de 2 numeros separados por new line
     */
    @Test
    public void sumaNumeroEnNewLine(){
        String lista = "100" + "\n200";
        int resultadoEsperado = 300;
        stringCalculator calculadora = new stringCalculator();
        int resultadoObtenido = calculadora.sumar(lista);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
     /**
     * retorna la suma de 3 numeros separados por new line y coma
     */
    @Test
    public void sumaNumeroConCaracteresMezclados(){
        String lista = "100\n200,50";
        int resultadoEsperado = 350;
        stringCalculator calculadora = new stringCalculator();
        int resultadoObtenido = calculadora.sumar(lista);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
     /**
     * retorna la suma de numeros de un String con separadores
     */
    @Test
    public void sumaNumeroConSeparadores(){
        String lista = "//#100//$200//#400//&-400";
        int resultadoEsperado = 300;
        stringCalculator calculadora = new stringCalculator();
        int resultadoObtenido = calculadora.sumar(lista);
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    
}
