import java.util.ArrayList;
import java.util.Arrays;
/**
 * Write a description of class stringCalculator here.
 * 
 * @author (Luciano Zamora) 
 * @version (5-3-2017)
 */
public class stringCalculator
{
    public int sumar (String listaNumeros){
        int resultado = 0;
        if((listaNumeros.length() > 1)){
            String limpia = listaNumeros.replaceAll("[^-?0-9]+", " "); //reemplaza todo elemento que no sea numerico por un espacio, solo permitiendo el - (para los negativos)
            ArrayList<String> numeros = new ArrayList<String>(Arrays.asList(limpia.trim().split(" ")));//se almacenan todos los numeros, retirando los espacios
             for(int x=0; x< numeros.size(); x++){
                    resultado = resultado + Integer.parseInt(numeros.get(x));// parsea y suma lo numeros encontrados
             }
        }
        return resultado;
    }
    
    /**
     * esto tenía antes de pensar (y guiarme *ping=stackoverflow) en una solución medianamente corta.
    public int sumar (String listaNumeros){
        int resultado = 0;
        //String[] numeros = listaNumeros.split("[,|\n|//]");
        if((listaNumeros.length() > 1) && !(listaNumeros.contains(","))){
            resultado = Integer.parseInt(listaNumeros);
        }else{
            ArrayList<String> numeros = new ArrayList<String>(Arrays.asList(listaNumeros.split(",")));
                if(!(listaNumeros.contains("\n"))){
                    for(int x=0; x< numeros.size(); x++){
                    resultado = resultado + Integer.parseInt(numeros.get(x));
                   }
                       
                }else{
                   resultado = sumaNewLines(listaNumeros);
                }
            } 
         return resultado;
        }
        
        public int sumaNewLines(String x){
        int resultado = 0;
        ArrayList<String> numeros = new ArrayList<String>(Arrays.asList(x.split("\n")));
            if(numeros.size() >= 2){
                for(int y=0; y< numeros.size(); y++){
                resultado = resultado + Integer.parseInt(numeros.get(y));
               }
            } 
        return resultado;
    }
     
    
    public int sumar (String listaNumeros){
        int resultado = 0;
        if((listaNumeros.length() > 1) && !(listaNumeros.contains(",")) ){
            resultado = Integer.parseInt(listaNumeros);
        }else{
            
        }
        return resultado;
    }
    */

    
      
    }
   
